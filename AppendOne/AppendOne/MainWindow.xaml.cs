﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace AppendOne
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }

        

        private void Button_Click_1(object sender, RoutedEventArgs e)
        {
            String name = tbName.Text;
            String age = tbAge.Text;
            int ageConvert = Convert.ToInt32(age);
            String gender = (rbMale.IsChecked==true ? "male" : (rbFemale.IsChecked == true ? "female" : "n/a" ));
            List<String> hobbiesList = new List<string>();

            if (cbBiking.IsChecked == true) {
                hobbiesList.Add("Bikin");
            }
            if (cbSwiming.IsChecked == true) {
                hobbiesList.Add("swiming");
            }
            if (cbOther.IsChecked == true) {
                hobbiesList.Add("other");
            }

            String hobbies = String.Join(",",hobbiesList);
            //verify name and age
            if (name.Length < 2 || name.Length > 250)
            {
                MessageBox.Show("not between 2 to 250");
                return;
            }
            else if (ageConvert < 1 || ageConvert > 150)
            {
                MessageBox.Show(" wrong age");
                return;
            }
            else {

                MessageBox.Show("saved");
                File.AppendAllText(@"C:\firsrog\data.txt",name+";"+age+";"+gender+";"+hobbies+Environment.NewLine);
            }
            //write out the result to te text file
        }
    }
}
